__all__ = ['foo']

from .mod2 import Bar
from pkg.sub_pkg_2 import mod3


def foo():
    print("[mod1]  foo()")


class Foo:
    pass
